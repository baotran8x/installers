FROM phusion/baseimage

ENV DEBIAN_FRONTEND noninteractive

# Install Java.
RUN apt-get update && apt-get install -y wget tar && apt-get install -y unzip && rm -rf /var/lib/apt/lists/*
RUN wget  --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u66-b17/jdk-8u66-linux-x64.tar.gz 
RUN mkdir -p /usr/lib/jvm/java-8-oracle
RUN tar -xzf jdk-8u66-linux-x64.tar.gz -C /usr/lib/jvm/java-8-oracle jdk1.8.0_66 --strip 1
RUN apt-get clean
RUN rm jdk-8u66-linux-x64.tar.gz


# Define commonly used JAVA_HOME variable
ENV JDK_HOME /usr/lib/jvm/java-8-oracle
ENV JRE_HOME ${JDK_HOME}/jre
ENV JAVA_HOME ${JRE_HOME}
ENV JAVA_ENDORSED_DIRS ${JDK_HOME}/lib/endorsed
ENV JAVA_OPTS "-Xms256m -Xmx2048m -XX:MaxPermSize=1024m -Djava.endorsed.dirs=${JAVA_ENDORSED_DIRS}"
ENV PATH .:${JRE_HOME}/bin:${JDK_HOME}/bin:${PATH}

# install JCE 8
RUN wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip" && \
    unzip jce_policy-8.zip -d /usr/lib/jvm/java-8-oracle/jre/lib/security && \
    cp /usr/lib/jvm/java-8-oracle/jre/lib/security/UnlimitedJCEPolicyJDK8/*.jar /usr/lib/jvm/java-8-oracle/jre/lib/security && \
    rm jce_policy-8.zip && rm -rf /usr/lib/jvm/java-8-oracle/jre/lib/security/UnlimitedJCEPolicyJDK8

WORKDIR /
# Define default command.
CMD ["bash"]
